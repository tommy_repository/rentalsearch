import {Observable, Subject} from 'rxjs';
import {LocationModel} from '../models/location.model';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';

export interface LocationService {
    searchObservable$: Subject<string>;
    find(params: any): Observable<LocationModel[]>;
    getSearchObservable(): Observable<LocationModel[]>;
}

export class DefaultLocationService implements LocationService {
    public searchObservable$ = new Subject<string>();

    find(params: any): Observable<LocationModel[]> {
        return new Observable<LocationModel[]>();
    }

    getSearchObservable(): Observable<LocationModel[]> {
        return this.searchObservable$
            .pipe(switchMap((term: string) => {
                return this.find({
                    solrTerm: term,
                });
            }));
    }
}
