import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {SearchComponent} from './components/search/search.component';
import { LocationRowComponent } from './components/location-row/location-row.component';
import { LocationContainerComponent } from './components/location-container/location-container.component';
import {PickupWidgetComponent} from './components/pickup-widget.component';
import {HighlightKeywordPipe} from './pipes/highlight-keyword.pipe';
import {FormsModule} from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
    ],
    exports: [
        PickupWidgetComponent,
    ],
    declarations: [
        PickupWidgetComponent,
        SearchComponent,
        LocationRowComponent,
        LocationContainerComponent,
        HighlightKeywordPipe,
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
    ],
    providers: [
        HighlightKeywordPipe,
    ]
})
export class PickupWidgetModule { }
