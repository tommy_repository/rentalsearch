import {PickupWidgetModule} from './pickup-widget.module';

describe('PickupLocationModule', () => {
    let pickupLocationModuleModule: PickupWidgetModule;

    beforeEach(() => {
        pickupLocationModuleModule = new PickupWidgetModule();
    });

    it('should create an instance', () => {
        expect(pickupLocationModuleModule).toBeTruthy();
    });
});
