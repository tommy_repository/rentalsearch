export interface Location {
    country: string;
    lng: number;
    city: string;
    searchType: string;
    alternative: string[];
    bookingId: string;
    placeType: string;
    placeKey: string;
    iata: string;
    countryIso: string;
    locationId: string;
    name: string;
    ufi: string;
    region: string;
    lang: string;
    lat: number;
}

export interface LocationMethods {
    getFullLocationName(): string;
    getFullRegionName(): string;
}

export class LocationModel implements Location, LocationMethods{
    static PLACE_TYPE = {
        A: 'Airport',
        C: 'City',
        S: 'Station',
    };

    get placeTypeName(): string {
        if (LocationModel.PLACE_TYPE[this.placeType] !== undefined) {
            this._placeTypeName = LocationModel.PLACE_TYPE[this.placeType];
        }

        /**
         * Was not sure how many place types there were, so
         * in case there are more then 3 (Airport, City, Station)
         * just fallback and extract placeTypeName from bookingId
         */
        if (this._placeTypeName === undefined) {
            this._placeTypeName = this.extractTypeFromBookingId(this.bookingId);
        }
        return this._placeTypeName;
    }

    set placeTypeName(value: string) {
        this._placeTypeName = value;
    }
    country: string;
    lng: number;
    city: string;
    alternative: string[];
    bookingId: string;
    countryIso: string;
    iata: string;
    lang: string;
    lat: number;
    locationId: string;
    name: string;
    placeKey: string;
    placeType: string;
    region: string;
    searchType: string;
    ufi: string;
    private _placeTypeName: string;

    constructor(locationData: Location) {
        for (const key in locationData) {
            if (locationData[key] === undefined) {
                continue;
            }

            const value = locationData[key];
            this[key] = value;
        }
    }

    getFullLocationName() {
        let iataPrepared = '';

        if (this.iata !== undefined) {
            iataPrepared = `(${this.iata})`;
        }

        return `${this.name} ${iataPrepared}`;
    }

    getFullRegionName() {
        const fullRegionNameArr = [];
        if (this.region !== undefined) {
            fullRegionNameArr.push(this.region);
        }

        if (this.country !== undefined) {
            fullRegionNameArr.push(this.country);
        }

        return `${fullRegionNameArr.join(', ')}`;
    }

    private extractTypeFromBookingId(bookingId: string): string {
        if (bookingId === undefined) {
            return '';
        }

        if (bookingId.indexOf('-') === -1) {
            return bookingId;
        }

        const type = bookingId.split('-')[0];
        return this.toUpperCase(type);
    }

    private toUpperCase(value: string) {
        return value.charAt(0).toUpperCase() + value.slice(1);
    }
}
