import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {LocationModel} from '../../models/location.model';

@Component({
    selector: 'app-location-row',
    templateUrl: './location-row.component.html',
    styleUrls: ['./location-row.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class LocationRowComponent implements OnInit {

    @Input()
    location: LocationModel;

    @Input()
    keywordToHighlight = '';
    rowIdIfActive = '';

    constructor() { }

    ngOnInit() {

    }

    getFullLocationName() {
        return this.location.getFullLocationName();
    }

    getFullRegionName() {
        return this.location.getFullRegionName();
    }

    setIdAsActive($event) {
        this.rowIdIfActive = 'location-row-active';
    }

    unsetId($event) {
        this.rowIdIfActive = '';
    }
}
