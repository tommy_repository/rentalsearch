import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationRowComponent } from './location-row.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {LocationModel} from '../../models/location.model';
import {HighlightKeywordPipe} from '../../pipes/highlight-keyword.pipe';

describe('LocationRowComponent', () => {
    let component: LocationRowComponent;
    let fixture: ComponentFixture<LocationRowComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                LocationRowComponent,
                HighlightKeywordPipe,
            ],
            schemas: [
                CUSTOM_ELEMENTS_SCHEMA,
            ],
            providers: [
                HighlightKeywordPipe,
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LocationRowComponent);
        component = fixture.componentInstance;
        component.location = new LocationModel({
            iata: 'MAN',
            name: 'Manchester Airport',
            country: 'United Kingdom',
            lng: 0,
            city: 'Some City',
            searchType: '',
            alternative: [],
            bookingId: 'airport-38566',
            placeType: '',
            placeKey: '',
            countryIso: '',
            locationId: '',
            ufi: '',
            region: 'Greater Manchester',
            lang: '',
            lat: 0,
        });
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
