import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationContainerComponent } from './location-container.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';

describe('LocationContainerComponent', () => {
    let component: LocationContainerComponent;
    let fixture: ComponentFixture<LocationContainerComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ LocationContainerComponent ],
            schemas: [
                CUSTOM_ELEMENTS_SCHEMA,
            ],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LocationContainerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
