import {Component, Input, OnInit} from '@angular/core';
import {LocationModel} from '../../models/location.model';

@Component({
    selector: 'app-location-container',
    templateUrl: './location-container.component.html',
    styleUrls: ['./location-container.component.scss']
})
export class LocationContainerComponent implements OnInit {

    @Input()
    locations: LocationModel[];

    @Input()
    keywordToHighlight = '';

    constructor() {
        this.locations = [];
    }

    ngOnInit() {
    }

}
