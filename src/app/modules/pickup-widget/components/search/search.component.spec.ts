import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchComponent } from './search.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {SearchComponentModel} from '../../models/search.component.model';
import {LocationApiServiceStub} from './stubs/location.api.service.stub';
import {HttpClientModule} from '@angular/common/http';
import {DefaultLocationService} from '../../services/DefaultLocationService';
import {FormsModule} from '@angular/forms';

describe('SearchInputComponent', () => {
    let component: SearchComponent;
    let fixture: ComponentFixture<SearchComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientModule,
                FormsModule,
            ],
            declarations: [ SearchComponent ],
            schemas: [
                CUSTOM_ELEMENTS_SCHEMA,
            ],
            providers: [
                {
                    provide: DefaultLocationService,
                    useClass: LocationApiServiceStub,
                },
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SearchComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it(`should have model with placeholder "city, airport, station region and district..."`, async(() => {
        const searchComponent = fixture.debugElement.componentInstance;
        expect(searchComponent.model).toEqual(jasmine.any(SearchComponentModel));
        expect(searchComponent.model.placeholder).toEqual('city, airport, station, region and district...');
    }));

    it(`should have model label "Pick-up location"`, async(() => {
        const pickupLocationComponent = fixture.debugElement.componentInstance;
        expect(pickupLocationComponent.model.label).toEqual('Pick-up location');
    }));
});
