import { Component, OnInit } from '@angular/core';
import {PickupLocationModelFactory} from '../../factories/pickup.location.model.factory';

import {SearchComponentModel} from '../../models/search.component.model';
import {DefaultLocationService} from '../../services/DefaultLocationService';
import {LocationModel} from '../../models/location.model';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {

    locations: LocationModel[] = [];
    model: SearchComponentModel;
    searchTerm = '';
    keywordToHighlight = '';
    loaderVisibility = false;

    constructor(private locationService: DefaultLocationService) {
        const factory = new PickupLocationModelFactory();
        this.model = factory.createSearchInputComponentModel();
    }

    ngOnInit() {
        this.initSearchObservable();
    }

    setNextTerm(term: string) {
        this.locationService.searchObservable$.next(term);
        this.keywordToHighlight = term;
    }

    private initSearchObservable() {
        this.locationService.getSearchObservable().subscribe((locations: LocationModel[]) => {
            this.loaderVisibility = false;
            this.locations = locations;
        });
    }

    updateSearch($event: string) {
        this.setNextTerm($event);

        if ($event.length > 1) {
            this.loaderVisibility = true;
            return ;
        }

        this.loaderVisibility = false;
    }
}
