import {Component, OnInit} from '@angular/core';
import {PickupLocationComponentModel} from '../models/pickup.location.component.model';
import {PickupLocationModelFactory} from '../factories/pickup.location.model.factory';

@Component({
    selector: 'app-pickup-widget',
    templateUrl: './pickup-widget.component.html',
    styleUrls: ['./pickup-widget.component.scss'],
})
export class PickupWidgetComponent implements OnInit {

    model: PickupLocationComponentModel;

    constructor() {
        const modelFactory = new PickupLocationModelFactory();
        this.model = modelFactory.createLocationPickupComponentModel();
    }

    ngOnInit() {
    }

}
