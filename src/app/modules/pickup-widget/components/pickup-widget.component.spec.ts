import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {PickupLocationComponentModel} from '../models/pickup.location.component.model';
import {PickupWidgetComponent} from './pickup-widget.component';

describe('PickupLocationComponent', () => {
    let component: PickupWidgetComponent;
    let fixture: ComponentFixture<PickupWidgetComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ PickupWidgetComponent ],
            schemas: [
                CUSTOM_ELEMENTS_SCHEMA,
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PickupWidgetComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it(`should have model header "Where are you going?"`, async(() => {
        const pickupLocationComponent = fixture.debugElement.componentInstance;
        expect(pickupLocationComponent.model).toEqual(jasmine.any(PickupLocationComponentModel));
        expect(pickupLocationComponent.model.title).toEqual('Where are you going?');
    }));
});
