import {PickupLocationComponentModel} from '../models/pickup.location.component.model';
import {SearchComponentModel} from '../models/search.component.model';

export class PickupLocationModelFactory {
    createLocationPickupComponentModel(): PickupLocationComponentModel {
        const model = new PickupLocationComponentModel();
        model.title = 'Where are you going?';

        return model;
    }

    createSearchInputComponentModel(): SearchComponentModel {
        const model = new SearchComponentModel();
        model.placeholder = 'city, airport, station, region and district...';
        model.label = 'Pick-up location';
        return model;
    }
}
