import { Pipe, PipeTransform } from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {SafeHtml} from '@angular/platform-browser/src/security/dom_sanitization_service';

export interface HighlightKeywordOptions {
    keyword: string;
    cssClass?: string;
}

/**
 * Expecting options format as follows (keyword,css-class)
 */
@Pipe({name: 'highlightKeyword'})
export class HighlightKeywordPipe implements PipeTransform {
    constructor(private sanitizer: DomSanitizer) {

    }
    transform(value: string, options: string): SafeHtml {
        const optionsArr = options.split(',');
        const formatedOptions: HighlightKeywordOptions = {
            keyword: optionsArr[0],
            cssClass: (optionsArr.length > 1) ? optionsArr[1] : 'highlight',
        };

        const highlightedHTML = value.replace(new RegExp(formatedOptions.keyword, 'gi'), match => {
            return '<span class="' + formatedOptions.cssClass + '">' + match + '</span>';
        });
        return this.sanitizer.bypassSecurityTrustHtml(highlightedHTML);
    }
}
