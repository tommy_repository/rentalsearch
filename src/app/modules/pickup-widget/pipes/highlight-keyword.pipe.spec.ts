import { HighlightKeywordPipe } from './highlight-keyword.pipe';
import {async, TestBed} from '@angular/core/testing';
import {PickupWidgetComponent} from '../components/pickup-widget.component';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

describe('SomethingPipe', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ PickupWidgetComponent ],
            schemas: [
                CUSTOM_ELEMENTS_SCHEMA,
            ],
        })
            .compileComponents();
    }));

    it('create an instance', () => {
        const domSanitizer = TestBed.get(DomSanitizer);
        const pipe = new HighlightKeywordPipe(domSanitizer);
        expect(pipe).toBeTruthy();
    });
});
