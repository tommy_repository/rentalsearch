import {LocationModel} from '../models/location.model';

describe('LocationModelTest', () => {
    let model: LocationModel;

    beforeEach(() => {
        model = new LocationModel({
            iata: 'MAN',
            name: 'Manchester Airport',
            country: 'United Kingdom',
            lng: 0,
            city: 'Some City',
            searchType: '',
            alternative: [],
            bookingId: 'airport-38566',
            placeType: '',
            placeKey: '',
            countryIso: '',
            locationId: '',
            ufi: '',
            region: 'Greater Manchester',
            lang: '',
            lat: 0,
        });
    });

    it('should create LocationModel with placeTypeName City when given model with placeType C', () => {
        model.placeType = 'C';

        expect(model.placeTypeName).toEqual('City');
    });

    it('should create LocationModel with placeTypeName Airport when given model without placeType', () => {
        expect(model.placeTypeName).toEqual('Airport');
    });

    it('should return valid full location name when calling getFullLocationName', () => {
        expect(model.getFullLocationName()).toEqual('Manchester Airport (MAN)');
    });

    it('should return valid full region name when calling getFullRegionName', () => {
        expect(model.getFullRegionName()).toEqual('Greater Manchester, United Kingdom');
    });

    it('should return valid full region name when calling getFullRegionName with undefined region', () => {
        model.region = undefined;
        expect(model.getFullRegionName()).toEqual('United Kingdom');
    });
});
