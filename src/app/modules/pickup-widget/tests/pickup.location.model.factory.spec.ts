import {PickupLocationModelFactory} from '../factories/pickup.location.model.factory';
import {PickupLocationComponentModel} from '../models/pickup.location.component.model';
import {SearchComponentModel} from '../models/search.component.model';

describe('PickupLocationModelFactory', () => {
    let factory: PickupLocationModelFactory;

    beforeEach(() => {
        factory = new PickupLocationModelFactory();
    });

    it('should create SearchInputComponentModel with valid data', () => {
        const model: SearchComponentModel = factory.createSearchInputComponentModel();
        expect(model.placeholder).toEqual('city, airport, station, region and district...');
    });

    it(`should create PickupLocationComponentModel with valid data`, (() => {
        const model: PickupLocationComponentModel = factory.createLocationPickupComponentModel();
        expect(model.title).toEqual('Where are you going?');
    }));
});
