import { BrowserModule } from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import {LocationApiService} from './services/location.service';
import {HttpClientModule} from '@angular/common/http';
import {DefaultLocationService} from './modules/pickup-widget/services/DefaultLocationService';
import {PickupWidgetModule} from './modules/pickup-widget/pickup-widget.module';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        PickupWidgetModule,
        HttpClientModule,
    ],
    providers: [
        {
            provide: DefaultLocationService,
            useClass: LocationApiService,
        }
    ],
    bootstrap: [AppComponent],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ]
})
export class AppModule { }
