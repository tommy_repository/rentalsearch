import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, switchMap} from 'rxjs/operators';
import {LocationService} from '../modules/pickup-widget/services/DefaultLocationService';
import {LocationModel} from '../modules/pickup-widget/models/location.model';
import {Subscriber} from 'rxjs/src/internal/Subscriber';

export interface ApiChangeableParams {
    solrRows?: number;
    solrTerm: string;
}

export interface ApiParams extends ApiChangeableParams {
    solrIndex: string;
}

export interface ApiResponse<T> {
    results: {
        isGooglePowered: boolean,
        docs: T[],
        numFound: number,
    };
}

export interface LocationApiResponse extends ApiResponse<LocationModel> {}

@Injectable({
    providedIn: 'root'
})
export class LocationApiService implements LocationService {
    static API_URL = 'https://cors.io/?https://www.rentalcars.com/FTSAutocomplete.do';
    static DEFAULT_ROWS_NUMBER = 6;

    public searchObservable$ = new Subject<string>();

    static createApiUrl(params: ApiChangeableParams): string {
        let rows = params.solrRows;

        if (rows === undefined) {
            rows = LocationApiService.DEFAULT_ROWS_NUMBER;
        }

        return `${LocationApiService.API_URL}?solrIndex=fts_en&solrRows=${rows}&solrTerm=${params.solrTerm}`;
    }

    constructor(private http: HttpClient) {
    }

    find(params: ApiChangeableParams): Observable<LocationModel[]> {

        return this.http.get(LocationApiService.createApiUrl(params))
            .pipe(map( (res: LocationApiResponse) => {
                if (res === null) {
                    return [];
                }

                return res.results.docs.map((location: LocationModel) => {
                    return new LocationModel(location);
                });
            }));
    }

    getSearchObservable(): Observable<LocationModel[]> {
        return this.searchObservable$
            .pipe(debounceTime(300))
            .pipe(distinctUntilChanged())
            .pipe(switchMap((term: string) => {

                if (term.length <= 1) {
                    return Observable.create((observer: Subscriber<LocationModel[]>) => {
                        observer.next([]);
                        observer.complete();
                    });
                }

                return this.find({
                    solrTerm: term,
                });
            }));
    }

}
