import {TestBed} from '@angular/core/testing';

import {ApiChangeableParams, LocationApiService} from './location.service';
import {HttpClientModule} from '@angular/common/http';
import {LocationModel} from '../modules/pickup-widget/models/location.model';

describe('LocationService', () => {
    let service: LocationApiService;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientModule,
            ],
            providers: [
                LocationApiService,
            ]
        });
        service = TestBed.get(LocationApiService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();

    });

    it('should create a valid url', () => {
        const params: ApiChangeableParams = {
            solrRows: 4,
            solrTerm: 'Manche',
        };
        expect(LocationApiService.createApiUrl(params))
            .toEqual('https://cors.io/?https://www.rentalcars.com/FTSAutocomplete.do?solrIndex=fts_en&solrRows=4&solrTerm=Manche');
    });

    it('should create a valid url when solrRows is undefined', () => {
        const params: ApiChangeableParams = {
            solrTerm: 'Manche',
        };
        expect(LocationApiService.createApiUrl(params))
            .toEqual('https://cors.io/?https://www.rentalcars.com/FTSAutocomplete.do?solrIndex=fts_en&solrRows=6&solrTerm=Manche');
    });

    it('[API]should return 1 location when solrTerm=Manch and solrRows=1', (done) => {
        const params: ApiChangeableParams = {
            solrTerm: 'Manche',
            solrRows: 1,
        };

        service.find(params).subscribe((locations: LocationModel[]) => {
            expect(locations.length).toEqual(1);
            done();
        });
    }, 5000);

    it('[API]should return empty array when solrTerm has less than 2 characters on getSearchObservable().subscribe()', (done) => {
        const solrTerm = 'M';
        const searchObservable = service.getSearchObservable();

        searchObservable.subscribe((locations: LocationModel[]) => {
            expect(locations.length).toEqual(0);
            done();
        });

        service.searchObservable$.next('M');
    }, 5000);
    it('[API]should return one LocationModel with name "No results found" when nothing found', (done) => {
        const params: ApiChangeableParams = {
            solrTerm: 'SomethingSomethingNotFound',
        };

        service.find(params).subscribe((locations: LocationModel[]) => {
            expect(locations.length).toEqual(1);
            expect(locations[0].name).toEqual('No results found');
            done();
        });
    }, 5000);
});
