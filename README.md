# RentalCar

### About


The application is created on `Angular 6` framework. It has only one module which can be easily used in an Angular application.


- Components have minimal logic and responsibilities
- Bussiness logic is extracted (it can be easily used in other frameworks)
- Main cases are covered with tests
- There is a bitbucket pipeline behind taking care of safe integrations


### Experimenting

Idea is to create in the future the same application with `React` using as much business logic as possible from this. Since business logic is decoupled from the framework with few tweaks it is easy to reuse everything and in `React` implement only `View Rendering` logic.

### Start application

Make sure that versions `node >= 8.9.0` and `npm >= 5.5.1` are installed.

```bash
npm install @angular/cli -g
```

```bash
npm install
```

```bash
npm start
```

Open your browser and check `http://localhost:4200/`.

### Run tests

```bash
npm test
```

Open your browser and check `http://localhost:9876/`.


### Technical description

##### Description

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.1.

##### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

##### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

##### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

##### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

##### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

##### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
